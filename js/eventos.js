const $tiempo = document.querySelector('.tiempo'),
$fecha = document.querySelector('.fecha');

function digitalClock(){
    let fecha = new Date(),
    dia = fecha.getDate(),
    mes = fecha.getMonth() + 1,
    anio = fecha.getFullYear(),
    diaSemana = fecha.getDay();

    dia = ('0' + dia).slice(-2);

    let ampm;
    let horas = fecha.getHours();
    let segundos = fecha.getSeconds();
    let minutos = fecha.getMinutes();

    let semana = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
    let showSemana = (semana[diaSemana]);

    let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]

    showMes = meses[mes];

    $fecha.innerHTML = `${showSemana} ${dia} de ${showMes} del ${anio}`

    if(horas >= 12) {
      horas = horas -12;
      ampm = "PM"
    }else {
      ampm="AM"
    }

    if(minutos<10){minutos = "0"+minutos}
    if(segundos<10){segundos = "0"+minutos}

    $tiempo.innerHTML = `${horas}:${minutos}:${segundos} ${ampm}`;
}
setInterval(() => {
    digitalClock()
}, 1000);